//
//  PersonalViewController.m
//  GitHubTrends
//
//  Created by Manuel Olmos Gil on 7/27/15.
//  Copyright (c) 2015 Manuel Olmos Gil. All rights reserved.
//

#import "PersonalViewController.h"
#import "GitHubApiClient.h"
#import "UserModel.h"
#import "WebViewController.h"
#import "AsyncImageView.h"

@interface PersonalViewController ()

@property (nonatomic) UserModel * actualUser;
@property (weak, nonatomic) IBOutlet UILabel *username;
@property (weak, nonatomic) IBOutlet UIButton *userUrl;
@property (weak, nonatomic) IBOutlet AsyncImageView * userAvatar;

@end

@implementation PersonalViewController

@synthesize actualUser, userUrl, username, userAvatar;

#pragma mark - View Lifecyle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"User details";
    
    [[GitHubApiClient sharedInstance] getUserInformation:@"manuelolmos" successBlock:^(id response, NSError * error) {
        
        if (response) {
            NSArray * items = response[@"items"];
            actualUser = [[UserModel alloc]initWithDictionary:[items firstObject]];
            username.text = actualUser.login;
            [userUrl setTitle:actualUser.html_url forState:UIControlStateNormal];
            
            userAvatar.image = [UIImage imageNamed:@"placeholder"];
            userAvatar.imageURL = [NSURL URLWithString:actualUser.avatar_url];

        }
        
    } failureBlock:^(NSError * error){
        [[[UIAlertView alloc]initWithTitle:@"Failed" message:@"Could not get the user information from Github" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]show];
    }];
}

#pragma mark - IBActions

- (IBAction)userUrlTUI:(id)sender {
    
    NSURL * url = [[NSURL alloc]initWithString:actualUser.html_url];
    WebViewController *webVC = [[WebViewController alloc] initWithIndexUrl:url];
    [self.navigationController pushViewController:webVC animated:YES];
}

@end
