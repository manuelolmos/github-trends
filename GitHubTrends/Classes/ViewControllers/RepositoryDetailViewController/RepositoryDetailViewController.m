//
//  RepositoryDetailViewController.m
//  ProjectTrends
//
//  Created by Manuel Olmos Gil on 7/17/15.
//  Copyright (c) 2015 Manuel Olmos Gil. All rights reserved.
//

#import "RepositoryDetailViewController.h"
#import "RepositoryModel.h"

@interface RepositoryDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *starsNumber;
@property (weak, nonatomic) IBOutlet UILabel *forksNumber;
@property (weak, nonatomic) IBOutlet UILabel *descriptionRepository;

@end

@implementation RepositoryDetailViewController

#pragma mark - ViewCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.repository.name;
    
    self.name.text = self.repository.name;
    self.starsNumber.text = [@(self.repository.stargazers_count) stringValue];
    self.forksNumber.text = [@(self.repository.forks) stringValue];
    self.descriptionRepository.text = self.repository.description;
}

@end
