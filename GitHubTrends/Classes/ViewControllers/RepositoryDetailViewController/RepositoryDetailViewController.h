//
//  RepositoryDetailViewController.h
//  ProjectTrends
//
//  Created by Manuel Olmos Gil on 7/17/15.
//  Copyright (c) 2015 Manuel Olmos Gil. All rights reserved.
//

@class RepositoryModel;

@interface RepositoryDetailViewController : UIViewController

@property (nonatomic) RepositoryModel * repository;

@end
