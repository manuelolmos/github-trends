//
//  WebViewController.h
//  PNMPlatform
//
//  Created by Adrian Ortuzar on 18/05/15.
//  Copyright (c) 2015 plusnewmedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

- (instancetype)initWithIndexHTMLPath:(NSString *)indexPath;
- (instancetype)initWithIndexUrl:(NSURL *)indexUrl;

@end
