//
//  WebViewController.m
//  PNMPlatform
//
//  Created by Adrian Ortuzar on 18/05/15.
//  Copyright (c) 2015 plusnewmedia. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@property (nonatomic, strong) NSString *indexPath;
@property (nonatomic, strong) NSURL *indexUrl;

@end

@implementation WebViewController

- (instancetype)initWithIndexHTMLPath:(NSString *)indexPath
{
    
    self = [self init];
    if (self) {
        self.indexPath = indexPath;
    }
    return self;
}

- (instancetype)initWithIndexUrl:(NSURL *)indexUrl
{
    
    self = [self init];
    if (self) {
        self.indexUrl = indexUrl;
    }
    return self;
}

-(instancetype)init{
    self = [super initWithNibName:@"WebViewController" bundle:nil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];    
    
    if  (self.indexPath){
        NSString *basePath = [self.indexPath stringByReplacingOccurrencesOfString:@"index.html" withString:@""];
        NSString* htmlString = [NSString stringWithContentsOfFile:self.indexPath encoding:NSUTF8StringEncoding error:nil];
        NSURL *baseUrl = [NSURL URLWithString:basePath];
        
        [self.webView loadHTMLString:htmlString baseURL:baseUrl];
    }
    else if(self.indexUrl){
        [self.webView loadRequest:[NSURLRequest requestWithURL:self.indexUrl]];
    }
}

@end
