//
//  HomeViewController.m
//  GitHubTrends
//
//  Created by Manuel Olmos Gil on 7/22/15.
//  Copyright (c) 2015 Manuel Olmos Gil. All rights reserved.
//

#import "HomeViewController.h"
#import "RepositoryListViewController.h"

@interface HomeViewController ()
@property (weak, nonatomic) IBOutlet UITextField *languageSelected;

@end

@implementation HomeViewController

#pragma mark - View Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"LanguageSelected"])
    {
        RepositoryListViewController *vc = [segue destinationViewController];
        vc.languageToSearch = self.languageSelected.text;
    }
}

@end
