//
//  PieChartViewController.m
//  GitHubTrends
//
//  Created by Manuel Olmos Gil on 7/23/15.
//  Copyright (c) 2015 Manuel Olmos Gil. All rights reserved.
//

#import "PieChartViewController.h"
#import <Charts/Charts.h>
#import "RepositoriesPerLanguageDataSource.h"

@interface PieChartViewController ()

@property (weak, nonatomic) IBOutlet PieChartView *pieChartView;
@property (nonatomic) RepositoriesPerLanguageDataSource * repositoriesDataSource;

@end

@implementation PieChartViewController

#pragma mark - View Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.repositoriesDataSource = [RepositoriesPerLanguageDataSource new];
    [self.repositoriesDataSource setupData:^(id response, NSError * error){
        [self setData];
    } failure:nil];
    
    [self setupPieChartView];
    [self.pieChartView animateWithXAxisDuration:1.5 yAxisDuration:1.5 easingOption:ChartEasingOptionEaseOutBack];
}

// This pragma mark should be encapsulated in another class. However it's not possible
// to subclass from swift class. Maybe i have to add it to a category. Use another library
// or take the change and implemente the project in swift
#pragma mark - BarChartView
- (void)setupPieChartView {
    
    self.pieChartView.usePercentValuesEnabled = YES;
    self.pieChartView.holeTransparent = YES;
    self.pieChartView.centerTextFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:12.f];
    self.pieChartView.holeRadiusPercent = 0.58;
    self.pieChartView.transparentCircleRadiusPercent = 0.61;
    self.pieChartView.descriptionText = @"";
    self.pieChartView.drawCenterTextEnabled = YES;
    self.pieChartView.drawHoleEnabled = YES;
    self.pieChartView.rotationAngle = 0.0;
    self.pieChartView.rotationEnabled = YES;
    self.pieChartView.centerText = @"Repositories per language";
    
    ChartLegend *l = self.pieChartView.legend;
    l.position = ChartLegendPositionBelowChartLeft;
    l.xEntrySpace = 7.0;
    l.yEntrySpace = 0.0;
    l.yOffset = 0.0;
}

- (void)setData {
    
    NSMutableArray * xVals = [NSMutableArray new];
    NSMutableArray *yVals = [NSMutableArray new];

    __block int i = 0;
    [self.repositoriesDataSource.repositoriesPerLanguage enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL* stop) {
        [xVals addObject:key];
        double yValue = [value doubleValue];
        [yVals addObject:[[BarChartDataEntry alloc] initWithValue:yValue xIndex:i]];
        i++;
    }];

    PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithYVals:yVals label:@"Languages"];
    dataSet.sliceSpace = 3.0;
    
    // add a lot of colors
    
    NSMutableArray *colors = [[NSMutableArray alloc] init];
    [colors addObjectsFromArray:ChartColorTemplates.vordiplom];
    [colors addObjectsFromArray:ChartColorTemplates.joyful];
    [colors addObjectsFromArray:ChartColorTemplates.colorful];
    [colors addObjectsFromArray:ChartColorTemplates.liberty];
    [colors addObjectsFromArray:ChartColorTemplates.pastel];
    [colors addObject:[UIColor colorWithRed:51/255.f green:181/255.f blue:229/255.f alpha:1.f]];
    
    dataSet.colors = colors;
    
    PieChartData *data = [[PieChartData alloc] initWithXVals:xVals dataSet:dataSet];
    
    NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
    pFormatter.numberStyle = NSNumberFormatterPercentStyle;
    pFormatter.maximumFractionDigits = 1;
    pFormatter.multiplier = @1.f;
    pFormatter.percentSymbol = @" %";
    [data setValueFormatter:pFormatter];
    [data setValueFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:11.f]];
    [data setValueTextColor:UIColor.whiteColor];
    
    self.pieChartView.data = data;
    [self.pieChartView highlightValues:nil];
}

@end
