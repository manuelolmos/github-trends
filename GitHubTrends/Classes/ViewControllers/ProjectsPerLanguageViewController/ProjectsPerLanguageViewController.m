//
//  ProjectsPerLanguageViewController.m
//  GitHubTrends
//
//  Created by Manuel Olmos Gil on 7/22/15.
//  Copyright (c) 2015 Manuel Olmos Gil. All rights reserved.
//

#import "ProjectsPerLanguageViewController.h"
#import <Charts/Charts.h>
#import "RepositoriesPerLanguageDataSource.h"

@interface ProjectsPerLanguageViewController ()

@property (weak, nonatomic) IBOutlet BarChartView *barChartView;
@property (nonatomic) RepositoriesPerLanguageDataSource * repositoriesDataSource;
@end

@implementation ProjectsPerLanguageViewController

@synthesize barChartView;

#pragma mark - View Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.repositoriesDataSource = [RepositoriesPerLanguageDataSource new];
    [self.repositoriesDataSource setupData:^(id response, NSError * error) {
        [self setData];
    } failure:nil];

    [self setupBarChartView];
}

// This pragma mark should be encapsulated in another class. However it's not possible to subclass from
// swift class. Maybe i have to add it to a category. Use another library or take the change and
// implemente the project in swift
#pragma mark - BarChartView
- (void)setupBarChartView {
    barChartView.descriptionText = @"";
    barChartView.noDataTextDescription = @"You need to provide data for the chart.";
    
    barChartView.drawBarShadowEnabled = NO;
    barChartView.drawValueAboveBarEnabled = YES;
    
    barChartView.maxVisibleValueCount = 60;
    barChartView.pinchZoomEnabled = NO;
    barChartView.drawGridBackgroundEnabled = NO;
    
    barChartView.legend.position = ChartLegendPositionBelowChartLeft;
    barChartView.legend.form = ChartLegendFormSquare;
    barChartView.legend.formSize = 9.0;
    barChartView.legend.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:11.f];
    barChartView.legend.xEntrySpace = 4.0;
    
    ChartXAxis *xAxis = barChartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont systemFontOfSize:10.f];
    xAxis.drawGridLinesEnabled = NO;
    xAxis.spaceBetweenLabels = 2.0;
    
    ChartYAxis *leftAxis = barChartView.leftAxis;
    leftAxis.labelFont = [UIFont systemFontOfSize:10.f];
    leftAxis.labelCount = 8;
    leftAxis.valueFormatter = [[NSNumberFormatter alloc] init];
    leftAxis.valueFormatter.maximumFractionDigits = 1;
    leftAxis.labelPosition = YAxisLabelPositionOutsideChart;
    leftAxis.spaceTop = 0.15;
    
    ChartYAxis *rightAxis = barChartView.rightAxis;
    rightAxis.drawGridLinesEnabled = NO;
    rightAxis.labelFont = [UIFont systemFontOfSize:10.f];
    rightAxis.labelCount = 8;
    rightAxis.valueFormatter = leftAxis.valueFormatter;
    rightAxis.spaceTop = 0.15;
}

- (void)setData {

    NSMutableArray * xVals = [NSMutableArray new];
    NSMutableArray *yVals = [NSMutableArray new];

    __block int i = 0;
    [self.repositoriesDataSource.repositoriesPerLanguage enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL* stop) {
        [xVals addObject:key];
        [yVals addObject:[[BarChartDataEntry alloc] initWithValue:[value doubleValue] xIndex:i]];
        i++;
    }];
    
    BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithYVals:yVals label:@"Languages"];
    set1.barSpace = 0.35;
    set1.colors = [NSArray arrayWithObjects:[UIColor redColor],[UIColor greenColor], [UIColor blueColor], [UIColor grayColor], [UIColor blackColor], [UIColor orangeColor], [UIColor brownColor], [UIColor yellowColor], nil];

    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    [dataSets addObject:set1];
    
    BarChartData *data = [[BarChartData alloc] initWithXVals:xVals dataSets:dataSets];
    [data setValueFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:10.f]];
    
    barChartView.data = data;
}

@end
