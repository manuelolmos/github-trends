//
//  RepositoryListViewController.m
//  GitHubTrends
//
//  Created by Manuel Olmos Gil on 7/22/15.
//  Copyright (c) 2015 Manuel Olmos Gil. All rights reserved.
//

#import "RepositoryListViewController.h"
#import "GitHubApiClient.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "RepositoriesDataSource.h"
#import "RepositoryModel.h"
#import "RepositoryDetailViewController.h"
#import "RepositoryCell.h"

@interface RepositoryListViewController ()

@property (unsafe_unretained, nonatomic) IBOutlet UITableView *repositoriesTableView;
@property (nonatomic) RepositoriesDataSource * repositoriesDataSource;

@end

@implementation RepositoryListViewController

#pragma mark - ViewCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.languageToSearch;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[GitHubApiClient sharedInstance]getRepositoriesWithLanguage:self.languageToSearch successBlock:^(id response, NSError * error){
        
        NSArray * items = response[@"items"];
        NSError * err;
        NSMutableArray * repositories = [NSMutableArray new];
        for (NSDictionary * dict in items) {
            RepositoryModel * repository = [[RepositoryModel alloc]initWithDictionary:dict error:&err];
            [repositories addObject:repository];
        }
        
        self.repositoriesDataSource = [[RepositoriesDataSource alloc]initWithItems:repositories];
        self.repositoriesTableView.dataSource = self.repositoriesDataSource;
        [self.repositoriesTableView reloadData];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    } failureBlock:^(NSError * error){
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
    
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    RepositoryCell * cell = sender;
    if ([[segue identifier] isEqualToString:@"RepositoryDetail"]) {
        RepositoryDetailViewController *vc = [segue destinationViewController];
        vc.repository = [self.repositoriesDataSource repositoryWithId:cell.repositoryId];
    }
}

@end
