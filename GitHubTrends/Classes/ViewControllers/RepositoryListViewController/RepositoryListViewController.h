//
//  RepositoryListViewController.h
//  GitHubTrends
//
//  Created by Manuel Olmos Gil on 7/22/15.
//  Copyright (c) 2015 Manuel Olmos Gil. All rights reserved.
//

@interface RepositoryListViewController : UIViewController<UITableViewDelegate>

@property (nonatomic, strong) NSString * languageToSearch;

@end
