//
//  RepositoryCell.h
//  ProjectTrends
//
//  Created by Manuel Olmos Gil on 7/17/15.
//  Copyright (c) 2015 Manuel Olmos Gil. All rights reserved.
//

@interface RepositoryCell : UITableViewCell

@property (weak, nonatomic) NSString * repositoryId;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *starsNumber;
@property (weak, nonatomic) IBOutlet UILabel *forksNumber;

@end
