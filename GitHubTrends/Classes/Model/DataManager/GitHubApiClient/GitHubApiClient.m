//
//  GitHubApiClient.m
//  ProjectTrends
//
//  Created by Manuel Olmos Gil on 7/17/15.
//  Copyright (c) 2015 Manuel Olmos Gil. All rights reserved.
//

#import "GitHubApiClient.h"
#import "AFHTTPRequestOperationManager.h"

@implementation GitHubApiClient

+ (instancetype)sharedInstance
{
    static dispatch_once_t p = 0;
    
    __strong static id _sharedObject = nil;
    
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    return _sharedObject;
}

- (void)getRepositoriesWithLanguage:(NSString*)language successBlock:(responseBlock)success failureBlock:(failureBlock)failure {
    
    //https://api.github.com/search/repositories?q=tetris+language:assembly&sort=stars&order=desc
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString * languageParameter = [NSString stringWithFormat:@"language:%@",language];
    NSDictionary * dic = @{@"q":languageParameter};
    
    NSString * requestURL = [NSString stringWithFormat:@"%@/repositories",kGithubUrl];
    
    [manager GET:requestURL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (success) {
            success(responseObject,nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (void)getNumberOfRepositoriesForLanguages:(NSArray*)languages successBlock:(responseBlock)success failureBlock:(failureBlock)failure {
    NSDictionary * reposPerLanguage =  @{@"Python" : @7, @"Swift" : @13, @"Ruby" : @20, @"Java" : @18, @"Erlang" : @10, @"Obj-c" : @12};

    if (success) {
        success(reposPerLanguage,nil);
    }
}

- (void)getUserInformation:(NSString*)username successBlock:(responseBlock)success failureBlock:(failureBlock)failure {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary * dic = @{@"q":username, @"type" : @"Users"};
    
    NSString * requestURL = [NSString stringWithFormat:@"%@/users",kGithubUrl];
    [manager GET:requestURL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (success) {
            success(responseObject,nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}
@end
