//
//  GitHubApiClient.h
//  ProjectTrends
//
//  Created by Manuel Olmos Gil on 7/17/15.
//  Copyright (c) 2015 Manuel Olmos Gil. All rights reserved.
//

typedef void (^failureBlock)(NSError *error);
typedef void (^responseBlock)(id response, NSError *error);

@interface GitHubApiClient : NSObject

+ (instancetype)sharedInstance;
- (void)getRepositoriesWithLanguage:(NSString*)language successBlock:(responseBlock)success failureBlock:(failureBlock)failure;
- (void)getNumberOfRepositoriesForLanguages:(NSArray*)languages successBlock:(responseBlock)success failureBlock:(failureBlock)failure;
- (void)getUserInformation:(NSString*)username successBlock:(responseBlock)success failureBlock:(failureBlock)failure;

@end
