//
//  UserModel.h
//  GitHubTrends
//
//  Created by Manuel Olmos Gil on 7/27/15.
//  Copyright (c) 2015 Manuel Olmos Gil. All rights reserved.
//

#import "JSONModel.h"

@interface UserModel : JSONModel

@property (nonatomic) NSString * login;
@property (nonatomic, assign) int id;
@property (nonatomic) NSString * avatar_url;
@property (nonatomic) NSString * html_url;
@property (nonatomic, assign) int score;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end
