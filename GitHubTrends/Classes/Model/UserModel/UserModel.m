//
//  UserModel.m
//  GitHubTrends
//
//  Created by Manuel Olmos Gil on 7/27/15.
//  Copyright (c) 2015 Manuel Olmos Gil. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel

@synthesize login,id,avatar_url,html_url,score;

-(instancetype)initWithDictionary:(NSDictionary*)dic {
    self = [super init];
    if (self) {
        login = dic[@"login"];
        self.id = [dic[@"id"] intValue];
        avatar_url = dic[@"avatar_url"];
        html_url = dic[@"html_url"];
        score = [dic[@"score"] intValue];
    }
    
    return self;
}
@end
