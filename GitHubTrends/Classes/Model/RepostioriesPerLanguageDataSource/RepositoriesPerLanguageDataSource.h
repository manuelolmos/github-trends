//
//  RepositoriesPerLanguageDataSource.h
//  GitHubTrends
//
//  Created by Manuel Olmos Gil on 7/23/15.
//  Copyright (c) 2015 Manuel Olmos Gil. All rights reserved.
//

#import "GitHubApiClient.h"

@interface RepositoriesPerLanguageDataSource : NSObject

@property (nonatomic) NSDictionary * repositoriesPerLanguage;

- (void)setupData:(responseBlock)success failure:(failureBlock)failure;

@end
