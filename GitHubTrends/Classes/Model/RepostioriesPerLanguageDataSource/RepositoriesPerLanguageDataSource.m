//
//  RepositoriesPerLanguageDataSource.m
//  GitHubTrends
//
//  Created by Manuel Olmos Gil on 7/23/15.
//  Copyright (c) 2015 Manuel Olmos Gil. All rights reserved.
//

#import "RepositoriesPerLanguageDataSource.h"

@implementation RepositoriesPerLanguageDataSource

@synthesize repositoriesPerLanguage;

- (void)setupData:(responseBlock)success failure:(failureBlock)failure {

    [[GitHubApiClient sharedInstance]getNumberOfRepositoriesForLanguages:@[@"Python",@"Ruby",@"Swift",@"Erlang",@"Java",@"C",@"C++"] successBlock:^(id response, NSError *error){
        
        if (response) {
            repositoriesPerLanguage = response;
        }
        
        if (success) {
            success(nil,nil);
        }
        
    } failureBlock:^(NSError *error){
        if (failure) {
            failure(error);
        }
    }];

}
@end
