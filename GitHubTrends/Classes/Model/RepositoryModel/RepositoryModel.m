//
//  RepositoryModel.m
//  ProjectTrends
//
//  Created by Manuel Olmos Gil on 7/17/15.
//  Copyright (c) 2015 Manuel Olmos Gil. All rights reserved.
//

#import "RepositoryModel.h"

@implementation RepositoryModel

@synthesize name,full_name,description,stargazers_count,forks,private;

-(instancetype)initWithDictionary:(NSDictionary*)dic {
    self = [super init];
    if (self) {
        name = dic[@"name"];
        self.id = dic[@"id"];
        full_name = dic[@"full_name"];
        description = dic[@"description"];
        stargazers_count = [dic[@"stargazers_count"] intValue];
        forks = [dic[@"forks"] intValue];
        private = [dic[@"private"] boolValue];
    }
    
    return self;
}

@end
