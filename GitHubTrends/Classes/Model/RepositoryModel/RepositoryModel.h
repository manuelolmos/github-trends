//
//  RepositoryModel.h
//  ProjectTrends
//
//  Created by Manuel Olmos Gil on 7/17/15.
//  Copyright (c) 2015 Manuel Olmos Gil. All rights reserved.
//

#import "JSONModel.h"

@interface RepositoryModel : JSONModel

@property (nonatomic) NSString * name;
@property (nonatomic) NSString * id;
@property (nonatomic) NSString * full_name;
@property (nonatomic) NSString * description;
@property (nonatomic, assign) int stargazers_count;
@property (nonatomic, assign) int forks;
@property (nonatomic, assign) BOOL private;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end
