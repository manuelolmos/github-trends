//
//  RepositoriesDataSource.m
//  ProjectTrends
//
//  Created by Manuel Olmos Gil on 7/17/15.
//  Copyright (c) 2015 Manuel Olmos Gil. All rights reserved.
//

#import "RepositoriesDataSource.h"
#import "RepositoryCell.h"
#import "RepositoryModel.h"

@implementation RepositoriesDataSource

@synthesize repositoriesArray;

#pragma mark - Life Cycle
-(instancetype)initWithItems:(NSArray*)items {
    self = [super init];
    if (self) {
        self.repositoriesArray = items;
    }
    return self;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return repositoriesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"RepositoryCell";

    RepositoryCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[RepositoryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    RepositoryModel * model = repositoriesArray[indexPath.row];
    cell.repositoryId = model.id;
    cell.name.text = model.name;
    cell.starsNumber.text = [@(model.stargazers_count) stringValue];
    cell.forksNumber.text = [@(model.forks) stringValue];
    
    return cell;
}

#pragma mark -
- (RepositoryModel*)repositoryWithId:(NSString*)repositoryId {
    __block RepositoryModel * repository;
    [self.repositoriesArray enumerateObjectsUsingBlock:^(RepositoryModel * model, NSUInteger idx, BOOL *stop){
        if ([model.id isEqualToString:repositoryId]) {
            repository = model;
            *stop = YES;
        }
    }];
    
    return repository;
}
@end
