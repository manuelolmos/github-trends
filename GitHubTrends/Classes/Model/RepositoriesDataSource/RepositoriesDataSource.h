//
//  RepositoriesDataSource.h
//  ProjectTrends
//
//  Created by Manuel Olmos Gil on 7/17/15.
//  Copyright (c) 2015 Manuel Olmos Gil. All rights reserved.
//

@class RepositoryModel;

@interface RepositoriesDataSource : NSObject<UITableViewDataSource>

@property (nonatomic) NSArray * repositoriesArray;

-(instancetype)initWithItems:(NSArray*)items;
- (RepositoryModel*)repositoryWithId:(NSString*)repositoryId;
@end
